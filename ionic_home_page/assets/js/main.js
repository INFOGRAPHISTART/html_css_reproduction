'use strict'

// $(window).on('load', function () {
//     $(".preloaded").fadeOut('slow');
// });

// $(window).on('load', function () {
//     $(".preloaded").fadeOut('slow');
// });
$(document).ready(function () {

    /*------------ Navbar Shrink --------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 100) {
            $('.navbar').addClass('navbar-shrink');
            // document.querySelector('.scrool-to').style.display = 'flex';
        } else {
            $('.navbar').removeClass('navbar-shrink');
            // document.querySelector('.scrool-to').style.display = 'none';
        }
    });

    var currentSelected = 0;

    const vueText = document.getElementsByClassName('vue');
    const vueImg = document.getElementById("banner-img");
    const angularText = document.getElementsByClassName('angular');
    const angularImg = document.getElementById("banner-img-1");
    const reactText = document.getElementsByClassName('react');
    const reactImg = document.getElementById("banner-img-2");

    setInterval(() => {
        if (currentSelected === 0) {
            reactText[0].style.display = 'none';
            reactText[0].style.opacity = '0';
            reactImg.style.display = 'none';
            reactImg.style.opacity = '0';




            vueText[0].style.display = 'inline-block';
            vueText[0].style.opacity = '1';
            vueImg.style.display = 'block';
            vueImg.style.opacity = '1';
            currentSelected = 1;
        }else if (currentSelected === 1) {
            vueText[0].style.display = 'none';
            vueText[0].style.opacity = '0';
            vueImg.style.display = 'none';
            vueImg.style.opacity = '0';

            angularText[0].style.display = 'inline-block';
            angularText[0].style.opacity = '1';
            angularImg.style.display = 'block';
            angularImg.style.opacity = '1';
            currentSelected = 2;
        }else if (currentSelected === 2) {
            angularText[0].style.display = 'none';
            angularText[0].style.opacity = '0';
            angularImg.style.display = 'none';
            angularImg.style.opacity = '0';



            reactText[0].style.display = 'inline-block';
            reactText[0].style.opacity = '1';
            reactImg.style.display = 'block';
            reactImg.style.opacity = '1';
            currentSelected = 0;
        }
        
    }, 10000);
   


    //    components previsualisation
    $('.previsualisation-header ul .ios').on('click', () => {
        const before = document.querySelector('.previsualisation-header div');
        before.style.left = '2%';
    });
    $('.previsualisation-header ul .android').on('click', () => {
        const before = document.querySelector('.previsualisation-header div');
        before.style.left = '52%';
    });
    // $('.previsualisation-header ul .android')


    // code preview menu list toggle
    let currentClass = ".card-code";
    let codeImgArray = [
        "./assets/images/how-img/Card_code.PNG",
        "./assets/images/how-img/btn_code.PNG",
        "./assets/images/how-img/Capture.PNG"
    ];
    let NativecodeImgArray = [
        "./assets/images/how-img/ideentity_code.PNG",
        "./assets/images/how-img/camera.PNG",
        "./assets/images/how-img/geolo.PNG"
    ];
    let NativeIconImgArray = [
        "./assets/images/how-img/icon-native-identityVault.png",
        "./assets/images/how-img/icon-native-camera.png",
        "./assets/images/how-img/icon-native-geolocation.png"
    ];
    let previewImgArray = [
        "./assets/images/how-img/card_preview.PNG",
        "./assets/images/how-img/btn_preview.PNG",
        "./assets/images/how-img/list_preview.PNG"
    ];
    const MenuLists = document.querySelectorAll(".code-item-menu");
    MenuLists.forEach((el) => {
        el.addEventListener('click', () => {
            document.querySelector(currentClass).classList.remove("active");
            currentClass = "." + el.classList[1];
            if (el.classList[1] == "card-code") {
                document.querySelector("#code_img").setAttribute("src", codeImgArray[0]);
                document.querySelector("#prev_img").setAttribute("src", previewImgArray[0]);
            } else if (el.classList[1] == "btn-code") {
                document.querySelector("#code_img").setAttribute("src", codeImgArray[1]);
                document.querySelector("#prev_img").setAttribute("src", previewImgArray[1]);
            }else if (el.classList[1] == "list-code") {
                document.querySelector("#code_img").setAttribute("src", codeImgArray[2]);
                document.querySelector("#prev_img").setAttribute("src", previewImgArray[2]);
            }
            el.classList.add("active");
        });
    });

    let nativecodeCurrent = ".identity-code";
    const NativeMenuLists = document.querySelectorAll(".funct-item-menu");
    NativeMenuLists.forEach((el) => {
        el.addEventListener('click', () => {
            document.querySelector(nativecodeCurrent).classList.remove("active");
            nativecodeCurrent = "." + el.classList[1];
            if (el.classList[1] == "identity-code") {
                document.querySelector("#native_code_img").setAttribute("src", NativecodeImgArray[0]);
                document.querySelector("#icon_code_preview").setAttribute("src", NativeIconImgArray[0]);
            } else if (el.classList[1] == "camera-code") {
                document.querySelector("#native_code_img").setAttribute("src", NativecodeImgArray[1]);
                document.querySelector("#icon_code_preview").setAttribute("src", NativeIconImgArray[1]);
            }else if (el.classList[1] == "geolocalisation-code") {
                document.querySelector("#native_code_img").setAttribute("src", NativecodeImgArray[2]);
                document.querySelector("#icon_code_preview").setAttribute("src", NativeIconImgArray[2]);
            }
            el.classList.add("active");
        });
    });


    // carousel
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin:0,
        nav: false,
        responsive:{
        0:{
            items:5
        },
        600:{
            items:5
        },
        1000:{
            items:5
        }
    }
    })

    // select effect on scroll
    const textArray = [
        "I was so inspired when I found Ionic. It fills a gap that’s missing when building for mobile and solves many complexities that otherwise require multiple libraries, keeping your code cleaner. Overall, it just makes mobile development fun and fast, so you can build more!",
        'Ionic dramatically changes the way mobile apps are built. Their integration with Angular means building functionality is now a breeze and it feels near-native. Your developers and your users will thank you.',
        'The more I look at Ionic, the more I love what they are doing. Truly. I wish I had a mobile app to build right now.',
        'Ionic is a shining example of a high-quality framework that takes advantage of Angular\'s power and flexibility, enabling developers to build production-ready mobile apps and Progressive Web Apps, in a fraction of the time.',
        'Ionic makes building cross-platform mobile apps enjoyable. Its integration with Angular is seamless, so it has easily become our go-to for mobile.'
    ];
    const left = 10;
    const width = 222;
    let currentItemSelect = 1;
    let currentItemClass = "r-item-1";
    const textReview = document.getElementsByClassName("review-text");
    let selectorElement = document.getElementsByClassName("selector-item");
    var reviewList = document.querySelectorAll(".review-item");
    reviewList.forEach(item => {
        item.addEventListener('click', () => {
            const step = item.getAttribute("data-index") - currentItemSelect;
            let left_step = (width * (step < 0 ? -step : step)) + left;
            if (left_step == 0) {
                left_step = left;
            }

            selectorElement[0].style.left = left_step + "px";
            textReview[0].textContent = textArray[item.getAttribute("data-index") - 1];
            item.classList.add("active");
            document.getElementsByClassName(currentItemClass)[0].classList.remove("active");
            currentItemClass = item.classList[1];
        });
    });




    // integration section
    const itList = document.querySelectorAll(".integration-item");
    itList.forEach((it) => {

        let rd = RandomNumber(0, 325);
        // it.style.bottom = rd + 'px';
        let right = RandomNumber(0, 125);
        let pass = false;
    
        setInterval(() => {
            right++;
            if (rd >= 325 || pass == true) {
                pass = true;
                rd--;
                if (rd <= 0) {
                    pass = false;
                    rd = 0;
                };
                it.style.transform = "translate3d(-"+right+"px,"+ rd +"px, 0px)";
            } else if (pass === false || rd == 0) {
                rd++;
                it.style.transform = "translate3d(-"+right+"px,"+rd+"px, 0px)";
            }
         }, 50);
    });
    // const it = document.querySelector(".integration-item");

    function RandomNumber(a = 0, b) {
        const step = Math.random() * b;
        return step;
    }
})


