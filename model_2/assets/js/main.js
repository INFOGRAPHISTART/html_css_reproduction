'use strict'

$(window).on('load', function () {
    $(".preloaded").fadeOut('slow');
});
// page scroll
$.scrollIt({
    topOffset: -50
})

const WHITE_LOGO = './assets/images/logo/white-logo.svg';
const BLACK_LOGO = './assets/images/logo/logo.svg';

$(window).on('load', function () {
    $(".preloaded").fadeOut('slow');
});
$(document).ready(function () {

    /*------------ Navbar Shrink --------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 90) {
            $('.header').addClass('navbar-shrink');
            $('.header-logo')[0].attributes[1].value = BLACK_LOGO;
            document.querySelector('.scrool-to').style.display = 'flex';
        } else {
            $('.header').removeClass('navbar-shrink');
            $('.header-logo')[0].attributes[1].value = WHITE_LOGO;
            document.querySelector('.scrool-to').style.display = 'none';
        }
    });

    /*--------------- testimonials carousel--------------*/
    $('.testimonials-carousel').owlCarousel({
        loop:true,
        autoplay: false,
        margin:0,
        responsiveClass:true,
        responsive:{
            0:{
                items: 1,
                // nav:true
            },
            600:{
                items: 1,
                // nav:true
            },
            1000:{
                items: 1,
                // nav:true
            }
        }
    })

    var mixer = mixitup('.box-list');

    $("#all").on("click", function(e){
        document.querySelectorAll('.categories ul li').forEach(el => {
            el.classList.remove('active')
        });
        e.target.classList.add('active')
    });
    $("#business").on("click", function(e){
        document.querySelectorAll('.categories ul li').forEach(el => {
            el.classList.remove('active')
        });
        e.target.classList.add('active')
    });
    $("#marketing").on("click", function(e){
        document.querySelectorAll('.categories ul li').forEach(el => {
            el.classList.remove('active')
        });
        e.target.classList.add('active')
    });
    $("#finance").on("click", function(e){
        document.querySelectorAll('.categories ul li').forEach(el => {
            el.classList.remove('active')
        });
        e.target.classList.add('active')
    });
    $("#consulting").on("click", function(e){
        document.querySelectorAll('.categories ul li').forEach(el => {
            el.classList.remove('active')
        });
        e.target.classList.add('active')
    });
});