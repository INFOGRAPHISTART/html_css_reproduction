const btn = document.querySelector(".navbar-toggle");
const menu = document.querySelector(".navbar-nav");
const navBar = document.querySelector(".navbar");

btn.addEventListener("click", (e)=>{
    if (btn.classList.contains("close")) {
        btn.classList.remove("close");
        menu.classList.remove("show");
    }else{
        btn.classList.add("close");
        menu.classList.add("show");
    }
})

document.addEventListener("scroll", (e) => {
    if (e.srcElement.scrollingElement.scrollTop > 88 ) {
        navBar.classList.add("shrin");
    }else{
        navBar.classList.remove("shrin");
    }
});